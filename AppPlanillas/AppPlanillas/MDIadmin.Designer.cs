﻿namespace AppPlanillas
{
    partial class MDIadmin
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIadmin));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.perfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verActivarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planillasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descuentosDeLeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otrosDescuentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otrosIngresosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrarPlanillasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrarCargosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrarUsuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.PaleTurquoise;
            this.menuStrip.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perfilesToolStripMenuItem,
            this.planillasToolStripMenuItem,
            this.reportesToolStripMenuItem,
            this.cargosToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.empleadosToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(835, 29);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // perfilesToolStripMenuItem
            // 
            this.perfilesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verActivarToolStripMenuItem,
            this.administrarToolStripMenuItem});
            this.perfilesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("perfilesToolStripMenuItem.Image")));
            this.perfilesToolStripMenuItem.Name = "perfilesToolStripMenuItem";
            this.perfilesToolStripMenuItem.Size = new System.Drawing.Size(95, 25);
            this.perfilesToolStripMenuItem.Text = "Perfiles";
            this.perfilesToolStripMenuItem.Click += new System.EventHandler(this.perfilesToolStripMenuItem_Click);
            // 
            // verActivarToolStripMenuItem
            // 
            this.verActivarToolStripMenuItem.Name = "verActivarToolStripMenuItem";
            this.verActivarToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.verActivarToolStripMenuItem.Text = "Ver-Activar";
            this.verActivarToolStripMenuItem.Click += new System.EventHandler(this.verActivarToolStripMenuItem_Click);
            // 
            // administrarToolStripMenuItem
            // 
            this.administrarToolStripMenuItem.Name = "administrarToolStripMenuItem";
            this.administrarToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.administrarToolStripMenuItem.Text = "Administrar-Insertar";
            this.administrarToolStripMenuItem.Click += new System.EventHandler(this.administrarToolStripMenuItem_Click);
            // 
            // planillasToolStripMenuItem
            // 
            this.planillasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.descuentosDeLeyToolStripMenuItem,
            this.otrosDescuentosToolStripMenuItem,
            this.otrosIngresosToolStripMenuItem,
            this.administrarPlanillasToolStripMenuItem});
            this.planillasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("planillasToolStripMenuItem.Image")));
            this.planillasToolStripMenuItem.Name = "planillasToolStripMenuItem";
            this.planillasToolStripMenuItem.Size = new System.Drawing.Size(103, 25);
            this.planillasToolStripMenuItem.Text = "Planillas";
            // 
            // descuentosDeLeyToolStripMenuItem
            // 
            this.descuentosDeLeyToolStripMenuItem.Name = "descuentosDeLeyToolStripMenuItem";
            this.descuentosDeLeyToolStripMenuItem.Size = new System.Drawing.Size(239, 26);
            this.descuentosDeLeyToolStripMenuItem.Text = "Descuentos de ley";
            this.descuentosDeLeyToolStripMenuItem.Click += new System.EventHandler(this.descuentosDeLeyToolStripMenuItem_Click);
            // 
            // otrosDescuentosToolStripMenuItem
            // 
            this.otrosDescuentosToolStripMenuItem.Name = "otrosDescuentosToolStripMenuItem";
            this.otrosDescuentosToolStripMenuItem.Size = new System.Drawing.Size(239, 26);
            this.otrosDescuentosToolStripMenuItem.Text = "Otros Descuentos";
            this.otrosDescuentosToolStripMenuItem.Click += new System.EventHandler(this.otrosDescuentosToolStripMenuItem_Click);
            // 
            // otrosIngresosToolStripMenuItem
            // 
            this.otrosIngresosToolStripMenuItem.Name = "otrosIngresosToolStripMenuItem";
            this.otrosIngresosToolStripMenuItem.Size = new System.Drawing.Size(239, 26);
            this.otrosIngresosToolStripMenuItem.Text = "Otros Ingresos";
            this.otrosIngresosToolStripMenuItem.Click += new System.EventHandler(this.otrosIngresosToolStripMenuItem_Click);
            // 
            // administrarPlanillasToolStripMenuItem
            // 
            this.administrarPlanillasToolStripMenuItem.Name = "administrarPlanillasToolStripMenuItem";
            this.administrarPlanillasToolStripMenuItem.Size = new System.Drawing.Size(239, 26);
            this.administrarPlanillasToolStripMenuItem.Text = "Administrar Planillas";
            this.administrarPlanillasToolStripMenuItem.Click += new System.EventHandler(this.administrarPlanillasToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportesToolStripMenuItem.Image")));
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(105, 25);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // cargosToolStripMenuItem
            // 
            this.cargosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrarCargosToolStripMenuItem1});
            this.cargosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cargosToolStripMenuItem.Image")));
            this.cargosToolStripMenuItem.Name = "cargosToolStripMenuItem";
            this.cargosToolStripMenuItem.Size = new System.Drawing.Size(90, 25);
            this.cargosToolStripMenuItem.Text = "Cargos";
            // 
            // administrarCargosToolStripMenuItem1
            // 
            this.administrarCargosToolStripMenuItem1.Name = "administrarCargosToolStripMenuItem1";
            this.administrarCargosToolStripMenuItem1.Size = new System.Drawing.Size(226, 26);
            this.administrarCargosToolStripMenuItem1.Text = "Administrar Cargos";
            this.administrarCargosToolStripMenuItem1.Click += new System.EventHandler(this.administrarCargosToolStripMenuItem1_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrarUsuariosToolStripMenuItem});
            this.usuariosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("usuariosToolStripMenuItem.Image")));
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(194, 25);
            this.usuariosToolStripMenuItem.Text = "Usuarios del sistema";
            // 
            // administrarUsuariosToolStripMenuItem
            // 
            this.administrarUsuariosToolStripMenuItem.Name = "administrarUsuariosToolStripMenuItem";
            this.administrarUsuariosToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.administrarUsuariosToolStripMenuItem.Text = "Administrar Usuarios";
            this.administrarUsuariosToolStripMenuItem.Click += new System.EventHandler(this.administrarUsuariosToolStripMenuItem_Click);
            // 
            // empleadosToolStripMenuItem
            // 
            this.empleadosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("empleadosToolStripMenuItem.Image")));
            this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
            this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(122, 25);
            this.empleadosToolStripMenuItem.Text = "Empleados";
            this.empleadosToolStripMenuItem.Click += new System.EventHandler(this.empleadosToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(835, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel.Text = "Estado";
            // 
            // MDIadmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(835, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIadmin";
            this.Text = "MDIadmin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIadmin_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem perfilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem planillasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verActivarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrarCargosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem administrarUsuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descuentosDeLeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otrosDescuentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otrosIngresosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrarPlanillasToolStripMenuItem;
    }
}



