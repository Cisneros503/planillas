﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdminPlanillascs : Form
    {
        public AdminPlanillascs()
        {
            InitializeComponent();
        }
        DatosPlanillaDataContext BDpla = new DatosPlanillaDataContext();

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BDpla.sp_listPlanillaXU(txtCodEmp.Text);
        }

        private void AdminPlanillascs_Load(object sender, EventArgs e)
        {

        }

        private void txtTipoPlani_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnListarTodas_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BDpla.sp_verPlanilla();
        }

        private void btnListXFecha_Click(object sender, EventArgs e)
        {
            
            try
            {
                string FechIni = txtFechIni.Text;
                string[] formato = { "yyyy-MM-dd" };
                DateTime fecha;
                if (DateTime.TryParseExact(FechIni,
                                           formato,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AssumeLocal,
                                           out fecha))
                {
                    //valid
                }
                dataGridView1.DataSource = BDpla.sp_listPlanillaXFECH(fecha);
               // MessageBox.Show("USUARIO IBORRADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try {
                int id;
                id = Convert.ToInt32(txtId.Text);
                BDpla.sp_deletePlanillaXID(id);
            MessageBox.Show("USUARIO IBORRADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtId.Text);
                string FechIni = txtFechIni.Text;
                string[] formato = { "yyyy-MM-dd" };
                DateTime fecha;
                if (DateTime.TryParseExact(FechIni,
                                           formato,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AssumeLocal,
                                           out fecha))
                {
                    //valid
                }
                string Fechfin = txtFechIni.Text;
                string[] format = { "yyyy-MM-dd" };
                DateTime fechafn;
                if (DateTime.TryParseExact(Fechfin,
                                           format,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AssumeLocal,
                                           out fechafn))
                {
                    //valid
                }
                decimal sueldo;
                sueldo = Convert.ToDecimal(txtSueldo.Text);
                decimal DescLey;
                DescLey = Convert.ToDecimal(txtDescLey.Text);
                decimal oDesc;
                decimal oIng;
                oIng = Convert.ToDecimal(txtOtrosIngr.Text);
                oDesc = Convert.ToDecimal(txtOtrosDesc.Text);
                BDpla.sp_updatePlanilla(id, txtCodEmp.Text, fecha, fechafn, txtTipoPlani.Text, sueldo, DescLey, oDesc, oIng );
                MessageBox.Show("USUARIO IBORRADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
            
        }
    }
}
