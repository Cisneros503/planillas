﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class MDIadmin : Form
    {
        private int childFormNumber = 0;

        public MDIadmin(string usr)
        {
            this.codUsr = usr;
            InitializeComponent();
        }
        public string codUsr; 
        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIadmin_Load(object sender, EventArgs e)
        {

        }

        private void perfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void verActivarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Perfil perf = new Perfil(this.codUsr);
            perf.MdiParent = this;
            perf.Show();
        }

        private void administrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistrarEmp RegEmp = new RegistrarEmp();
            RegEmp.MdiParent = this;
            RegEmp.Show();
        }

        private void administrarCargosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void administrarCargosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AdminCargo admCargo = new AdminCargo();
            admCargo.MdiParent = this;
            admCargo.Show();
        }

        private void administrarUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdmUsrs usrs = new AdmUsrs();
            usrs.MdiParent = this;
            usrs.Show();
        }

        private void descuentosDeLeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdmDescLey DescLey = new AdmDescLey();
            DescLey.MdiParent = this;
            DescLey.Show();
        }

        private void otrosDescuentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminDesc Desc = new AdminDesc();
            Desc.MdiParent = this;
            Desc.Show();
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistrarEmp regEmp = new RegistrarEmp();
            regEmp.MdiParent = this;
            regEmp.Show();
        }

        private void otrosIngresosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminIngresos adming = new AdminIngresos();
            adming.MdiParent = this;
            adming.Show();
        }

        private void administrarPlanillasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminPlanillascs aPla = new AdminPlanillascs();
            aPla.MdiParent = this;
            aPla.Show();
        }
    }
}
