﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class Perfil : Form
    {
        public Perfil(string usr)
        {
            this.codUsr = usr;
            InitializeComponent();
            
        }
        public string codUsr;
        PerfilesDataContext perfilDataContext = new PerfilesDataContext();
        private void Perfil_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'planillasNVDataSet.Empleado' Puede moverla o quitarla según sea necesario.
            //this.empleadoTableAdapter.Fill(this.planillasNVDataSet.Empleado);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = this.perfilDataContext.sp_empleados();
            dataGridView1.Visible = true;
            dataGridView1.ReadOnly = false;
        }

        private void btnBuscarCodEmp_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("codigo: " + txtBuscarCodEmp.Text) ;
            dataGridView1.DataSource = this.perfilDataContext.sp_perfEmpleado(txtBuscarCodEmp.Text);
            dataGridView1.Visible = true;
            txtEditEstadoCod.Text = txtBuscarCodEmp.Text;
        }

        private void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = this.perfilDataContext.sp_perfEmpNombre(txtBuscarNombre.Text);
            dataGridView1.Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCambiarEst_Click(object sender, EventArgs e)
        {
            

            try
            {
                int est;
                est = 0;
                if (comboBox1.Text == "Activo")
                {
                    est = 1;
                }
                else if (comboBox1.Text == "Inactivo")
                {
                    est = 0;
                }
                else
                {
                    MessageBox.Show("El estado del usuario Ahora es INACTIVO");
                }
            
            dataGridView1.DataSource = this.perfilDataContext.editarEstado(est, txtEditEstadoCod.Text);
            MessageBox.Show("Estado modificado con exito!!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }
    }
}
