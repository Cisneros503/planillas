﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdminDesc : Form
    {
        public AdminDesc()
        {
            InitializeComponent();
        }
        DataOtrosDescDataContext BDotrosDesc = new DataOtrosDescDataContext();
        private void AdminDesc_Load(object sender, EventArgs e)
        {

        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            try
            {

                int id;
                id = Convert.ToInt32(txtIdDesc.Text);
                var perfData = BDotrosDesc.sp_listOtrosDescXId(id);
                foreach (var item in perfData)
                {
                    txtIdDesc.Text = item.id.ToString();
                    txtCodEmp.Text = item.codigoEmpleado;
                    txtNomDesc.Text = item.nombre;
                    txtMonto.Text = item.montoDesc.ToString();
                    txtPlazo.Text = item.plazo.ToString();
                    if (item.estado == 1)
                    {
                        cbbEstado.Text = "Activo";
                    }
                    else
                    { cbbEstado.Text = "Inactivo"; }
                    
                    /*Double sueldo;
                    sueldo = Convert.ToDouble(item.sueldo);

                    txtSueldo.Text = sueldo.ToString();*/
                }
                //asignando valores a los txt
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BDotrosDesc.sp_listarOtrosDesc();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            { 
                int id;
                id = Convert.ToInt32(txtIdDesc.Text);
                decimal desc;
                desc = Convert.ToDecimal(txtMonto.Text);
                int plazo;
                plazo = Convert.ToInt32(txtPlazo.Text);
                int est;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else
                { est = 0; }
                BDotrosDesc.sp_editOtrosDesc(txtCodEmp.Text, id, txtNomDesc.Text, desc, plazo, est );
                MessageBox.Show("Modificado con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
}

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            { 
            int id;
            id = Convert.ToInt32(txtIdDesc.Text);
            BDotrosDesc.sp_deleteOtrosDesc(id);
            MessageBox.Show("Borrado con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
}

        private void btnBuscarXU_Click(object sender, EventArgs e)
        {
            try
            { 
            dataGridView1.DataSource = BDotrosDesc.sp_listOtrosDescXU(txtCodEmp.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
}

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtIdDesc.Text);
                decimal desc;
                desc = Convert.ToDecimal(txtMonto.Text);
                int plazo;
                plazo = Convert.ToInt32(txtPlazo.Text);
                int est;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else
                { est = 0; }
                BDotrosDesc.sp_insertOtrosDesc(txtCodEmp.Text, id, txtNomDesc.Text, desc, plazo, est);
                MessageBox.Show("Insertó con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }
    }
}
