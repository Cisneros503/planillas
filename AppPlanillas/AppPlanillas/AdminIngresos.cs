﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdminIngresos : Form
    {
        public AdminIngresos()
        {
            InitializeComponent();
        }
        DatosingresosDataContext BDingresos = new DatosingresosDataContext();
        private void AdminIngresos_Load(object sender, EventArgs e)
        {

        }

        private void btnListIngresos_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BDingresos.sp_listOtrosIngre();
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtIdIngreso.Text);
                var perfData = BDingresos.sp_listOtrosIngreXID(id);
                foreach (var item in perfData)
                {
                    txtIdIngreso.Text = item.id.ToString();
                    txtCodEmp.Text = item.codigoEmpleado;
                    txtNomIngreso.Text = item.nombre;
                    txtMontoIng.Text = item.montoIngreso.ToString();
                    txtPlazo.Text = item.plazo.ToString();
                    if (item.estado == 1)
                    {
                        cbbEstado.Text = "Activo";
                    }
                    else
                    { cbbEstado.Text = "Inactivo"; }

                    /*Double sueldo;
                    sueldo = Convert.ToDouble(item.sueldo);

                    txtSueldo.Text = sueldo.ToString();*/
                }
                //asignando valores a los txt
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnIngresoxUsr_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BDingresos.sp_listOtrosIngreXU(txtCodEmp.Text);
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
            int id;
            id = Convert.ToInt32(txtIdIngreso.Text);
            BDingresos.sp_deleteOtrosIngre(id);
                MessageBox.Show("Borrado con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtIdIngreso.Text);
                decimal desc;
                desc = Convert.ToDecimal(txtMontoIng.Text);
                int plazo;
                plazo = Convert.ToInt32(txtPlazo.Text);
                int est;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else
                { est = 0; }
                BDingresos.sp_insertOtrosIngre(txtCodEmp.Text, id, txtNomIngreso.Text, desc, plazo, est);
                MessageBox.Show("Insertó con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtIdIngreso.Text);
                decimal desc;
                desc = Convert.ToDecimal(txtMontoIng.Text);
                int plazo;
                plazo = Convert.ToInt32(txtPlazo.Text);
                int est;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else
                { est = 0; }
                BDingresos.sp_updateOtrosIngre(txtCodEmp.Text, id, txtNomIngreso.Text, desc, plazo, est);
                MessageBox.Show("Modificado con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }
    }
}
