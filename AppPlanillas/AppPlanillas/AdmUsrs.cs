﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdmUsrs : Form
    {
        public AdmUsrs()
        {
            InitializeComponent();
        }
        UsuariosDataContext DBusr = new UsuariosDataContext();
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DBusr.sp_listUsr();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DBusr.sp_deleteUsr(txtCodigo.Text);
            MessageBox.Show("Borrado con exito!");
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                DBusr.sp_EditUsr(txtCodigo.Text,txtNicUsr.Text, txtPass.Text,txtNombreUsr.Text,txtPrivilegios.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                DBusr.sp_insertUsr(txtCodigo.Text, txtNicUsr.Text, txtPass.Text, txtNombreUsr.Text, txtPrivilegios.Text);
                MessageBox.Show("inserto el cargo con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnMostrarUsr_Click(object sender, EventArgs e)
        {
            try
            {
                var usrData = DBusr.sp_selectUsr(txtCodigo.Text);
                foreach (var item in usrData)
                {
                    txtCodigo.Text = item.codigoEmpleado;
                    txtNicUsr.Text = item.usuario;
                    txtPass.Text = item.password;
                    txtNombreUsr.Text = item.nombre;
                    txtPrivilegios.Text = item.rol;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void AdmUsrs_Load(object sender, EventArgs e)
        {

        }
    }
}
