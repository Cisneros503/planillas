﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppPlanillas
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PlanillasNV")]
	public partial class DatosPlanillaDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definiciones de métodos de extensibilidad
    partial void OnCreated();
    #endregion
		
		public DatosPlanillaDataContext() : 
				base(global::AppPlanillas.Properties.Settings.Default.PlanillasNVConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DatosPlanillaDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DatosPlanillaDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DatosPlanillaDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DatosPlanillaDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Planilla> Planilla
		{
			get
			{
				return this.GetTable<Planilla>();
			}
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_deletePlanillaXID")]
		public int sp_deletePlanillaXID([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> id)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), id);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_insertPlanilla")]
		public int sp_insertPlanilla([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> id, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string codEmp, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechaIni, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechafin, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(75)")] string tipoPlanilla, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> salario, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> descLey, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> otrosDesc, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> otrosIng)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), id, codEmp, fechaIni, fechafin, tipoPlanilla, salario, descLey, otrosDesc, otrosIng);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_listPlanillaXFECH")]
		public ISingleResult<sp_listPlanillaXFECHResult> sp_listPlanillaXFECH([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechaIni)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), fechaIni);
			return ((ISingleResult<sp_listPlanillaXFECHResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_listPlanillaXID")]
		public ISingleResult<sp_listPlanillaXIDResult> sp_listPlanillaXID([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> id)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), id);
			return ((ISingleResult<sp_listPlanillaXIDResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_updatePlanilla")]
		public int sp_updatePlanilla([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> id, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string codEmp, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechaIni, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechafin, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(75)")] string tipoPlanilla, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> salario, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> descLey, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> otrosDesc, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> otrosIng)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), id, codEmp, fechaIni, fechafin, tipoPlanilla, salario, descLey, otrosDesc, otrosIng);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_verPlanilla")]
		public ISingleResult<sp_verPlanillaResult> sp_verPlanilla()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<sp_verPlanillaResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_listPlanillaXU")]
		public ISingleResult<sp_listPlanillaXUResult> sp_listPlanillaXU([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string codEmp)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), codEmp);
			return ((ISingleResult<sp_listPlanillaXUResult>)(result.ReturnValue));
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Planilla")]
	public partial class Planilla
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaPlanillaInicio;
		
		private System.DateTime _fechaPlanillaFin;
		
		private string _tipoPlanilla;
		
		private System.Nullable<decimal> _Salario;
		
		private decimal _DescuentosLey;
		
		private System.Nullable<decimal> _OtrosDescuentos;
		
		private System.Nullable<decimal> _OtrosIngresos;
		
		public Planilla()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaInicio", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaInicio
		{
			get
			{
				return this._fechaPlanillaInicio;
			}
			set
			{
				if ((this._fechaPlanillaInicio != value))
				{
					this._fechaPlanillaInicio = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaFin", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaFin
		{
			get
			{
				return this._fechaPlanillaFin;
			}
			set
			{
				if ((this._fechaPlanillaFin != value))
				{
					this._fechaPlanillaFin = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_tipoPlanilla", DbType="VarChar(25) NOT NULL", CanBeNull=false)]
		public string tipoPlanilla
		{
			get
			{
				return this._tipoPlanilla;
			}
			set
			{
				if ((this._tipoPlanilla != value))
				{
					this._tipoPlanilla = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Salario", DbType="Money")]
		public System.Nullable<decimal> Salario
		{
			get
			{
				return this._Salario;
			}
			set
			{
				if ((this._Salario != value))
				{
					this._Salario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DescuentosLey", DbType="Money NOT NULL")]
		public decimal DescuentosLey
		{
			get
			{
				return this._DescuentosLey;
			}
			set
			{
				if ((this._DescuentosLey != value))
				{
					this._DescuentosLey = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosDescuentos", DbType="Money")]
		public System.Nullable<decimal> OtrosDescuentos
		{
			get
			{
				return this._OtrosDescuentos;
			}
			set
			{
				if ((this._OtrosDescuentos != value))
				{
					this._OtrosDescuentos = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosIngresos", DbType="Money")]
		public System.Nullable<decimal> OtrosIngresos
		{
			get
			{
				return this._OtrosIngresos;
			}
			set
			{
				if ((this._OtrosIngresos != value))
				{
					this._OtrosIngresos = value;
				}
			}
		}
	}
	
	public partial class sp_listPlanillaXFECHResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaPlanillaInicio;
		
		private System.DateTime _fechaPlanillaFin;
		
		private string _tipoPlanilla;
		
		private System.Nullable<decimal> _Salario;
		
		private decimal _DescuentosLey;
		
		private System.Nullable<decimal> _OtrosDescuentos;
		
		private System.Nullable<decimal> _OtrosIngresos;
		
		public sp_listPlanillaXFECHResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaInicio", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaInicio
		{
			get
			{
				return this._fechaPlanillaInicio;
			}
			set
			{
				if ((this._fechaPlanillaInicio != value))
				{
					this._fechaPlanillaInicio = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaFin", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaFin
		{
			get
			{
				return this._fechaPlanillaFin;
			}
			set
			{
				if ((this._fechaPlanillaFin != value))
				{
					this._fechaPlanillaFin = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_tipoPlanilla", DbType="VarChar(25) NOT NULL", CanBeNull=false)]
		public string tipoPlanilla
		{
			get
			{
				return this._tipoPlanilla;
			}
			set
			{
				if ((this._tipoPlanilla != value))
				{
					this._tipoPlanilla = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Salario", DbType="Money")]
		public System.Nullable<decimal> Salario
		{
			get
			{
				return this._Salario;
			}
			set
			{
				if ((this._Salario != value))
				{
					this._Salario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DescuentosLey", DbType="Money NOT NULL")]
		public decimal DescuentosLey
		{
			get
			{
				return this._DescuentosLey;
			}
			set
			{
				if ((this._DescuentosLey != value))
				{
					this._DescuentosLey = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosDescuentos", DbType="Money")]
		public System.Nullable<decimal> OtrosDescuentos
		{
			get
			{
				return this._OtrosDescuentos;
			}
			set
			{
				if ((this._OtrosDescuentos != value))
				{
					this._OtrosDescuentos = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosIngresos", DbType="Money")]
		public System.Nullable<decimal> OtrosIngresos
		{
			get
			{
				return this._OtrosIngresos;
			}
			set
			{
				if ((this._OtrosIngresos != value))
				{
					this._OtrosIngresos = value;
				}
			}
		}
	}
	
	public partial class sp_listPlanillaXIDResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaPlanillaInicio;
		
		private System.DateTime _fechaPlanillaFin;
		
		private string _tipoPlanilla;
		
		private System.Nullable<decimal> _Salario;
		
		private decimal _DescuentosLey;
		
		private System.Nullable<decimal> _OtrosDescuentos;
		
		private System.Nullable<decimal> _OtrosIngresos;
		
		public sp_listPlanillaXIDResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaInicio", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaInicio
		{
			get
			{
				return this._fechaPlanillaInicio;
			}
			set
			{
				if ((this._fechaPlanillaInicio != value))
				{
					this._fechaPlanillaInicio = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaFin", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaFin
		{
			get
			{
				return this._fechaPlanillaFin;
			}
			set
			{
				if ((this._fechaPlanillaFin != value))
				{
					this._fechaPlanillaFin = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_tipoPlanilla", DbType="VarChar(25) NOT NULL", CanBeNull=false)]
		public string tipoPlanilla
		{
			get
			{
				return this._tipoPlanilla;
			}
			set
			{
				if ((this._tipoPlanilla != value))
				{
					this._tipoPlanilla = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Salario", DbType="Money")]
		public System.Nullable<decimal> Salario
		{
			get
			{
				return this._Salario;
			}
			set
			{
				if ((this._Salario != value))
				{
					this._Salario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DescuentosLey", DbType="Money NOT NULL")]
		public decimal DescuentosLey
		{
			get
			{
				return this._DescuentosLey;
			}
			set
			{
				if ((this._DescuentosLey != value))
				{
					this._DescuentosLey = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosDescuentos", DbType="Money")]
		public System.Nullable<decimal> OtrosDescuentos
		{
			get
			{
				return this._OtrosDescuentos;
			}
			set
			{
				if ((this._OtrosDescuentos != value))
				{
					this._OtrosDescuentos = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosIngresos", DbType="Money")]
		public System.Nullable<decimal> OtrosIngresos
		{
			get
			{
				return this._OtrosIngresos;
			}
			set
			{
				if ((this._OtrosIngresos != value))
				{
					this._OtrosIngresos = value;
				}
			}
		}
	}
	
	public partial class sp_verPlanillaResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaPlanillaInicio;
		
		private System.DateTime _fechaPlanillaFin;
		
		private string _tipoPlanilla;
		
		private System.Nullable<decimal> _Salario;
		
		private decimal _DescuentosLey;
		
		private System.Nullable<decimal> _OtrosDescuentos;
		
		private System.Nullable<decimal> _OtrosIngresos;
		
		public sp_verPlanillaResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaInicio", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaInicio
		{
			get
			{
				return this._fechaPlanillaInicio;
			}
			set
			{
				if ((this._fechaPlanillaInicio != value))
				{
					this._fechaPlanillaInicio = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaFin", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaFin
		{
			get
			{
				return this._fechaPlanillaFin;
			}
			set
			{
				if ((this._fechaPlanillaFin != value))
				{
					this._fechaPlanillaFin = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_tipoPlanilla", DbType="VarChar(25) NOT NULL", CanBeNull=false)]
		public string tipoPlanilla
		{
			get
			{
				return this._tipoPlanilla;
			}
			set
			{
				if ((this._tipoPlanilla != value))
				{
					this._tipoPlanilla = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Salario", DbType="Money")]
		public System.Nullable<decimal> Salario
		{
			get
			{
				return this._Salario;
			}
			set
			{
				if ((this._Salario != value))
				{
					this._Salario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DescuentosLey", DbType="Money NOT NULL")]
		public decimal DescuentosLey
		{
			get
			{
				return this._DescuentosLey;
			}
			set
			{
				if ((this._DescuentosLey != value))
				{
					this._DescuentosLey = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosDescuentos", DbType="Money")]
		public System.Nullable<decimal> OtrosDescuentos
		{
			get
			{
				return this._OtrosDescuentos;
			}
			set
			{
				if ((this._OtrosDescuentos != value))
				{
					this._OtrosDescuentos = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosIngresos", DbType="Money")]
		public System.Nullable<decimal> OtrosIngresos
		{
			get
			{
				return this._OtrosIngresos;
			}
			set
			{
				if ((this._OtrosIngresos != value))
				{
					this._OtrosIngresos = value;
				}
			}
		}
	}
	
	public partial class sp_listPlanillaXUResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaPlanillaInicio;
		
		private System.DateTime _fechaPlanillaFin;
		
		private string _tipoPlanilla;
		
		private System.Nullable<decimal> _Salario;
		
		private decimal _DescuentosLey;
		
		private System.Nullable<decimal> _OtrosDescuentos;
		
		private System.Nullable<decimal> _OtrosIngresos;
		
		public sp_listPlanillaXUResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaInicio", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaInicio
		{
			get
			{
				return this._fechaPlanillaInicio;
			}
			set
			{
				if ((this._fechaPlanillaInicio != value))
				{
					this._fechaPlanillaInicio = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaPlanillaFin", DbType="DateTime NOT NULL")]
		public System.DateTime fechaPlanillaFin
		{
			get
			{
				return this._fechaPlanillaFin;
			}
			set
			{
				if ((this._fechaPlanillaFin != value))
				{
					this._fechaPlanillaFin = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_tipoPlanilla", DbType="VarChar(25) NOT NULL", CanBeNull=false)]
		public string tipoPlanilla
		{
			get
			{
				return this._tipoPlanilla;
			}
			set
			{
				if ((this._tipoPlanilla != value))
				{
					this._tipoPlanilla = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Salario", DbType="Money")]
		public System.Nullable<decimal> Salario
		{
			get
			{
				return this._Salario;
			}
			set
			{
				if ((this._Salario != value))
				{
					this._Salario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DescuentosLey", DbType="Money NOT NULL")]
		public decimal DescuentosLey
		{
			get
			{
				return this._DescuentosLey;
			}
			set
			{
				if ((this._DescuentosLey != value))
				{
					this._DescuentosLey = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosDescuentos", DbType="Money")]
		public System.Nullable<decimal> OtrosDescuentos
		{
			get
			{
				return this._OtrosDescuentos;
			}
			set
			{
				if ((this._OtrosDescuentos != value))
				{
					this._OtrosDescuentos = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OtrosIngresos", DbType="Money")]
		public System.Nullable<decimal> OtrosIngresos
		{
			get
			{
				return this._OtrosIngresos;
			}
			set
			{
				if ((this._OtrosIngresos != value))
				{
					this._OtrosIngresos = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
