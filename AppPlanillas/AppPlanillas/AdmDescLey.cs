﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdmDescLey : Form
    {
        public AdmDescLey()
        {
            InitializeComponent();
        }
        DescLeyDataContext dbDescLey = new DescLeyDataContext();
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = this.dbDescLey.sp_listarDescLey();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                
                int id;
                id = Convert.ToInt32(txtId.Text);
                decimal porcentaje;
                porcentaje = Convert.ToDecimal(txtPorcentaje.Text);
                //Este llama al sp que me permite insertar el descuento
                this.dbDescLey.sp_insertDescLey(id, txtNomDescLey.Text, porcentaje);
                MessageBox.Show("DESCUENTO INSERTADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int id;
                id = Convert.ToInt32(txtId.Text);
                decimal porcentaje;
                porcentaje = Convert.ToDecimal(txtPorcentaje.Text);
                //Este llama al sp que me permite MODIFICAR el descuento
                this.dbDescLey.sp_updateDescLey(id, txtNomDescLey.Text, porcentaje);
                MessageBox.Show("DESCUENTO MODIFICADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                int id;
                id = Convert.ToInt32(txtId.Text);
                //Este llama al sp que me permite insertar el descuento
                this.dbDescLey.sp_deleteDescLey(id);
                MessageBox.Show("DESCUENTO ELIMINADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }
    }
}
