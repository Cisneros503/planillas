﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class AdminCargo : Form
    {
        public AdminCargo()
        {
            InitializeComponent();
        }
        CargosDataContextDataContext cargoData = new CargosDataContextDataContext();
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnMostrarCargo_Click(object sender, EventArgs e)
        {
            try
            {
                var perfData = cargoData.sp_verCargo(txtNombre.Text);
                foreach (var item in perfData)
                {
                    Double sueldo;  
                    txtNombre.Text = item.nombreCargo ;
                    txtId.Text = item.idCargo.ToString();
                    sueldo = Convert.ToDouble(item.sueldoBase);
                    txtSueldo.Text = sueldo.ToString();
                    txtDescrip.Text = item.Descripcion.ToString();

                    /*Double sueldo;
                    sueldo = Convert.ToDouble(item.sueldo);

                    txtSueldo.Text = sueldo.ToString();*/
                }
                //asignando valores a los txt
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {       
                int id; //txtId.Text
                id = Convert.ToInt32(txtId.Text);
                decimal sueldo;
                sueldo = Convert.ToDecimal(txtSueldo.Text);
                cargoData.sp_updateCargo(id, txtNombre.Text, sueldo, txtDescrip.Text);
                MessageBox.Show("Modificó con exito!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
            
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            int id; //txtId.Text
            id = Convert.ToInt32(txtId.Text);
            decimal sueldo;
            sueldo = Convert.ToDecimal(txtSueldo.Text);
            cargoData.sp_insertCargo(id, txtNombre.Text, sueldo, txtDescrip.Text);
            MessageBox.Show("inserto el cargo con exito!");
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {   
                dataGridView1.DataSource = cargoData.sp_listCargos();
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            cargoData.sp_deleteCargo(txtNombre.Text);
            MessageBox.Show("Borrado con exito!");
        }

        private void AdminCargo_Load(object sender, EventArgs e)
        {

        }
    }
}
