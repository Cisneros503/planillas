﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class RegistrarEmp : Form
    {
        public RegistrarEmp()
        {
            InitializeComponent();
        }
        PerfilesDataContext perfilDataContext = new PerfilesDataContext();
        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                //convirtiendo el dato del estado para poderlo escribir en BD
                int est;
                est = 0;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else if (cbbEstado.Text == "Inactivo")
                {
                    est = 0;
                }
                else {
                    MessageBox.Show("El Usuario ha quedado como inactivo!! ");
                }
                //convirtiendo el dato del genero para poderlo escribir en BD
                int gen;
                gen = 1;
                if (cbbGenero.Text == "Hombre")
                {
                    gen = 1;
                }
                else if (cbbGenero.Text == "Mujer")
                {
                    gen = 0;
                }

                ///ESTE CODIGO ME CONVIERTE LAS FECHAS
                string str = txtFingreso.Text;
                string[] format = { "yyyy-MM-dd" };
                DateTime date;
                if (DateTime.TryParseExact(str,
                                           format,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AdjustToUniversal,
                                           out date))
                {
                    //valid
                }
                string Fnac = txtFnac.Text;
                string[] formato = { "yyyy-MM-dd" };
                DateTime fecha;
                if (DateTime.TryParseExact(Fnac,
                                           formato,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AssumeLocal,
                                           out fecha))
                {
                    //valid
                }
                int telefono;
                telefono = Convert.ToInt32(txtTelefono.Text);
                decimal sueldo;
                sueldo = Convert.ToDecimal(txtSueldo.Text);
                string dui;
                dui = txtDui.Text;
                string nit;
                nit = txtNit.Text;
                string isss;
                isss = txtIsss.Text;
                string afp;
                afp = txtAfp.Text;
 
                //Este llama al sp que me permite insertar al empleado

                perfilDataContext.sp_insertEmpleado(txtCodEmp.Text, date,
                    txtNomb1.Text, txtNomb2.Text, txtApe1.Text, txtApe2.Text, fecha,
                    telefono, dui, nit, isss, afp, gen,
                    txtCargo.Text, sueldo, est);

                MessageBox.Show("USUARIO INSERTADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void dtpFingreso_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void RegistrarEmp_Load(object sender, EventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                //convirtiendo el dato del estado para poderlo escribir en BD
                int est;
                est = 0;
                if (cbbEstado.Text == "Activo")
                {
                    est = 1;
                }
                else if (cbbEstado.Text == "Inactivo")
                {
                    est = 0;
                }
                else
                {
                    MessageBox.Show("El Usuario ha quedado como inactivo!! ");
                }
                //convirtiendo el dato del genero para poderlo escribir en BD
                int gen;
                gen = 1;
                if (cbbGenero.Text == "Hombre")
                {
                    gen = 1;
                }
                else if (cbbGenero.Text == "Mujer")
                {
                    gen = 0;
                }

                ///ESTE CODIGO ME CONVIERTE LAS FECHAS
                string str = txtFingreso.Text;
                string[] format = { "yyyy-MM-dd" };
                DateTime date;
                if (DateTime.TryParseExact(str,
                                           format,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AdjustToUniversal,
                                           out date))
                {
                    //valid
                }
                string Fnac = txtFnac.Text;
                string[] formato = { "yyyy-MM-dd" };
                DateTime fecha;
                if (DateTime.TryParseExact(Fnac,
                                           formato,
                                           System.Globalization.CultureInfo.InvariantCulture,
                                           System.Globalization.DateTimeStyles.AssumeLocal,
                                           out fecha))
                {
                    //valid
                }
                int telefono;
                telefono = Convert.ToInt32(txtTelefono.Text);
                decimal sueldo;
                sueldo = Convert.ToDecimal(txtSueldo.Text);
                string dui;
                dui = txtDui.Text;
                string nit;
                nit = txtNit.Text;
                string isss;
                isss = txtIsss.Text;
                string afp;
                afp = txtAfp.Text;

                //Este llama al sp que me permite insertar al empleado

                perfilDataContext.sp_updateEmp(txtCodEmp.Text, date,
                    txtNomb1.Text, txtNomb2.Text, txtApe1.Text, txtApe2.Text, fecha,
                    telefono, dui, nit, isss, afp, gen,
                    txtCargo.Text, sueldo, est);

                MessageBox.Show("USUARIO MODIFICADO CON EXITO...");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                var perfData = perfilDataContext.sp_perfEmpleado(txtCodEmp.Text);
                foreach (var item in perfData)
                {
                    Double sueldo;
                    txtFingreso.Text = item.fechaIngreso.ToString();
                    txtNomb1.Text = item.nombre;
                    txtNomb2 .Text = item.apellido1;
                    txtApe1.Text = item.apellido1;
                    txtApe2.Text = item.apellido2;
                    txtFnac.Text = item.fechaNacimiento.ToString();
                    txtIsss.Text = item.isss;
                    txtAfp.Text = item.afp;
                    txtCargo.Text = item.Cargo;
                    txtTelefono.Text = item.telefono.ToString();
                    txtDui.Text = item.dui;
                    txtNit.Text = item.nit;
                    sueldo = Convert.ToDouble(item.sueldo);
                    txtSueldo.Text = sueldo.ToString();
                    if (item.estado == 1)
                    {
                        cbbEstado.Text = "Activo";
                    }
                    else
                    { cbbEstado.Text = "Inactivo"; }
                    if (item.genero == 1)
                    {
                        cbbGenero.Text = "Hombre";
                    }
                    else
                    { cbbGenero.Text = "Mujer"; }

                }
                //asignando valores a los txt
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                perfilDataContext.sp_deleteEmp(txtCodEmp.Text);
                MessageBox.Show("USUARIO ELIMINADO CON EXITO");
            }
            catch (Exception)
            {
                MessageBox.Show("Error con el acceso a datos... favor intente nuevamente");
            }
}

        private void btnVerTodos_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = perfilDataContext.sp_verEmpleados();
        }
    }
}
