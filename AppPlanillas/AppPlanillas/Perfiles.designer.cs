﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppPlanillas
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PlanillasNV")]
	public partial class PerfilesDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definiciones de métodos de extensibilidad
    partial void OnCreated();
    #endregion
		
		public PerfilesDataContext() : 
				base(global::AppPlanillas.Properties.Settings.Default.PlanillasNVConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public PerfilesDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public PerfilesDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public PerfilesDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public PerfilesDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Empleado> Empleado
		{
			get
			{
				return this.GetTable<Empleado>();
			}
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_perfEmpleado")]
		public ISingleResult<sp_perfEmpleadoResult> sp_perfEmpleado([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(25)")] string codEmp)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), codEmp);
			return ((ISingleResult<sp_perfEmpleadoResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_empleados")]
		public ISingleResult<sp_empleadosResult> sp_empleados()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<sp_empleadosResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_perfEmpNombre")]
		public ISingleResult<sp_perfEmpNombreResult> sp_perfEmpNombre([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(100)")] string nomEmp)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), nomEmp);
			return ((ISingleResult<sp_perfEmpNombreResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.editarEstado")]
		public int editarEstado([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> est, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(25)")] string cod)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), est, cod);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_deleteEmp")]
		public int sp_deleteEmp([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(25)")] string codEmp)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), codEmp);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_insertEmpleado")]
		public int sp_insertEmpleado(
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(20)")] string codEmp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechaIngreso, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nombreEmp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nombreEmp2, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string apellido1, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string apel2, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fNac, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> telefono, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string dui, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nit, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string isss, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string afp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> genero, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(25)")] string cargo, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> sueldo, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> estado)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), codEmp, fechaIngreso, nombreEmp, nombreEmp2, apellido1, apel2, fNac, telefono, dui, nit, isss, afp, genero, cargo, sueldo, estado);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_verEmpleados")]
		public ISingleResult<sp_verEmpleadosResult> sp_verEmpleados()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<sp_verEmpleadosResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_updateEmp")]
		public int sp_updateEmp(
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(20)")] string codEmp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fechaIngreso, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nombreEmp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nombreEmp2, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string apellido1, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string apel2, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] System.Nullable<System.DateTime> fNac, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> telefono, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string dui, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string nit, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string isss, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(50)")] string afp, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> genero, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="VarChar(25)")] string cargo, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Money")] System.Nullable<decimal> sueldo, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> estado)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), codEmp, fechaIngreso, nombreEmp, nombreEmp2, apellido1, apel2, fNac, telefono, dui, nit, isss, afp, genero, cargo, sueldo, estado);
			return ((int)(result.ReturnValue));
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Empleado")]
	public partial class Empleado
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaIngreso;
		
		private string _nombre;
		
		private string _nombre2;
		
		private string _apellido1;
		
		private string _apellido2;
		
		private System.Nullable<System.DateTime> _fechaNacimiento;
		
		private System.Nullable<int> _telefono;
		
		private string _dui;
		
		private string _nit;
		
		private string _isss;
		
		private string _afp;
		
		private System.Nullable<int> _genero;
		
		private string _Cargo;
		
		private decimal _sueldo;
		
		private int _estado;
		
		public Empleado()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaIngreso", DbType="DateTime NOT NULL")]
		public System.DateTime fechaIngreso
		{
			get
			{
				return this._fechaIngreso;
			}
			set
			{
				if ((this._fechaIngreso != value))
				{
					this._fechaIngreso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre2", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre2
		{
			get
			{
				return this._nombre2;
			}
			set
			{
				if ((this._nombre2 != value))
				{
					this._nombre2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido1", DbType="VarChar(100)")]
		public string apellido1
		{
			get
			{
				return this._apellido1;
			}
			set
			{
				if ((this._apellido1 != value))
				{
					this._apellido1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido2", DbType="VarChar(100)")]
		public string apellido2
		{
			get
			{
				return this._apellido2;
			}
			set
			{
				if ((this._apellido2 != value))
				{
					this._apellido2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaNacimiento", DbType="DateTime")]
		public System.Nullable<System.DateTime> fechaNacimiento
		{
			get
			{
				return this._fechaNacimiento;
			}
			set
			{
				if ((this._fechaNacimiento != value))
				{
					this._fechaNacimiento = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="Int")]
		public System.Nullable<int> telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dui", DbType="VarChar(20)")]
		public string dui
		{
			get
			{
				return this._dui;
			}
			set
			{
				if ((this._dui != value))
				{
					this._dui = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="VarChar(30)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isss", DbType="VarChar(30)")]
		public string isss
		{
			get
			{
				return this._isss;
			}
			set
			{
				if ((this._isss != value))
				{
					this._isss = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_afp", DbType="VarChar(30)")]
		public string afp
		{
			get
			{
				return this._afp;
			}
			set
			{
				if ((this._afp != value))
				{
					this._afp = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_genero", DbType="Int")]
		public System.Nullable<int> genero
		{
			get
			{
				return this._genero;
			}
			set
			{
				if ((this._genero != value))
				{
					this._genero = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cargo", DbType="VarChar(40) NOT NULL", CanBeNull=false)]
		public string Cargo
		{
			get
			{
				return this._Cargo;
			}
			set
			{
				if ((this._Cargo != value))
				{
					this._Cargo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sueldo", DbType="Money NOT NULL")]
		public decimal sueldo
		{
			get
			{
				return this._sueldo;
			}
			set
			{
				if ((this._sueldo != value))
				{
					this._sueldo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_estado", DbType="Int NOT NULL")]
		public int estado
		{
			get
			{
				return this._estado;
			}
			set
			{
				if ((this._estado != value))
				{
					this._estado = value;
				}
			}
		}
	}
	
	public partial class sp_perfEmpleadoResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaIngreso;
		
		private string _nombre;
		
		private string _nombre2;
		
		private string _apellido1;
		
		private string _apellido2;
		
		private System.Nullable<System.DateTime> _fechaNacimiento;
		
		private System.Nullable<int> _telefono;
		
		private string _dui;
		
		private string _nit;
		
		private string _isss;
		
		private string _afp;
		
		private System.Nullable<int> _genero;
		
		private string _Cargo;
		
		private decimal _sueldo;
		
		private int _estado;
		
		public sp_perfEmpleadoResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaIngreso", DbType="DateTime NOT NULL")]
		public System.DateTime fechaIngreso
		{
			get
			{
				return this._fechaIngreso;
			}
			set
			{
				if ((this._fechaIngreso != value))
				{
					this._fechaIngreso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre2", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre2
		{
			get
			{
				return this._nombre2;
			}
			set
			{
				if ((this._nombre2 != value))
				{
					this._nombre2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido1", DbType="VarChar(100)")]
		public string apellido1
		{
			get
			{
				return this._apellido1;
			}
			set
			{
				if ((this._apellido1 != value))
				{
					this._apellido1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido2", DbType="VarChar(100)")]
		public string apellido2
		{
			get
			{
				return this._apellido2;
			}
			set
			{
				if ((this._apellido2 != value))
				{
					this._apellido2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaNacimiento", DbType="DateTime")]
		public System.Nullable<System.DateTime> fechaNacimiento
		{
			get
			{
				return this._fechaNacimiento;
			}
			set
			{
				if ((this._fechaNacimiento != value))
				{
					this._fechaNacimiento = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="Int")]
		public System.Nullable<int> telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dui", DbType="VarChar(20)")]
		public string dui
		{
			get
			{
				return this._dui;
			}
			set
			{
				if ((this._dui != value))
				{
					this._dui = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="VarChar(30)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isss", DbType="VarChar(30)")]
		public string isss
		{
			get
			{
				return this._isss;
			}
			set
			{
				if ((this._isss != value))
				{
					this._isss = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_afp", DbType="VarChar(30)")]
		public string afp
		{
			get
			{
				return this._afp;
			}
			set
			{
				if ((this._afp != value))
				{
					this._afp = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_genero", DbType="Int")]
		public System.Nullable<int> genero
		{
			get
			{
				return this._genero;
			}
			set
			{
				if ((this._genero != value))
				{
					this._genero = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cargo", DbType="VarChar(40) NOT NULL", CanBeNull=false)]
		public string Cargo
		{
			get
			{
				return this._Cargo;
			}
			set
			{
				if ((this._Cargo != value))
				{
					this._Cargo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sueldo", DbType="Money NOT NULL")]
		public decimal sueldo
		{
			get
			{
				return this._sueldo;
			}
			set
			{
				if ((this._sueldo != value))
				{
					this._sueldo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_estado", DbType="Int NOT NULL")]
		public int estado
		{
			get
			{
				return this._estado;
			}
			set
			{
				if ((this._estado != value))
				{
					this._estado = value;
				}
			}
		}
	}
	
	public partial class sp_empleadosResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaIngreso;
		
		private string _nombre;
		
		private string _nombre2;
		
		private string _apellido1;
		
		private string _apellido2;
		
		private System.Nullable<System.DateTime> _fechaNacimiento;
		
		private System.Nullable<int> _telefono;
		
		private string _dui;
		
		private string _nit;
		
		private string _isss;
		
		private string _afp;
		
		private System.Nullable<int> _genero;
		
		private string _Cargo;
		
		private decimal _sueldo;
		
		private int _estado;
		
		public sp_empleadosResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaIngreso", DbType="DateTime NOT NULL")]
		public System.DateTime fechaIngreso
		{
			get
			{
				return this._fechaIngreso;
			}
			set
			{
				if ((this._fechaIngreso != value))
				{
					this._fechaIngreso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre2", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre2
		{
			get
			{
				return this._nombre2;
			}
			set
			{
				if ((this._nombre2 != value))
				{
					this._nombre2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido1", DbType="VarChar(100)")]
		public string apellido1
		{
			get
			{
				return this._apellido1;
			}
			set
			{
				if ((this._apellido1 != value))
				{
					this._apellido1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido2", DbType="VarChar(100)")]
		public string apellido2
		{
			get
			{
				return this._apellido2;
			}
			set
			{
				if ((this._apellido2 != value))
				{
					this._apellido2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaNacimiento", DbType="DateTime")]
		public System.Nullable<System.DateTime> fechaNacimiento
		{
			get
			{
				return this._fechaNacimiento;
			}
			set
			{
				if ((this._fechaNacimiento != value))
				{
					this._fechaNacimiento = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="Int")]
		public System.Nullable<int> telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dui", DbType="VarChar(20)")]
		public string dui
		{
			get
			{
				return this._dui;
			}
			set
			{
				if ((this._dui != value))
				{
					this._dui = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="VarChar(30)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isss", DbType="VarChar(30)")]
		public string isss
		{
			get
			{
				return this._isss;
			}
			set
			{
				if ((this._isss != value))
				{
					this._isss = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_afp", DbType="VarChar(30)")]
		public string afp
		{
			get
			{
				return this._afp;
			}
			set
			{
				if ((this._afp != value))
				{
					this._afp = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_genero", DbType="Int")]
		public System.Nullable<int> genero
		{
			get
			{
				return this._genero;
			}
			set
			{
				if ((this._genero != value))
				{
					this._genero = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cargo", DbType="VarChar(40) NOT NULL", CanBeNull=false)]
		public string Cargo
		{
			get
			{
				return this._Cargo;
			}
			set
			{
				if ((this._Cargo != value))
				{
					this._Cargo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sueldo", DbType="Money NOT NULL")]
		public decimal sueldo
		{
			get
			{
				return this._sueldo;
			}
			set
			{
				if ((this._sueldo != value))
				{
					this._sueldo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_estado", DbType="Int NOT NULL")]
		public int estado
		{
			get
			{
				return this._estado;
			}
			set
			{
				if ((this._estado != value))
				{
					this._estado = value;
				}
			}
		}
	}
	
	public partial class sp_perfEmpNombreResult
	{
		
		private int _id;
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaIngreso;
		
		private string _nombre;
		
		private string _nombre2;
		
		private string _apellido1;
		
		private string _apellido2;
		
		private System.Nullable<System.DateTime> _fechaNacimiento;
		
		private System.Nullable<int> _telefono;
		
		private string _dui;
		
		private string _nit;
		
		private string _isss;
		
		private string _afp;
		
		private System.Nullable<int> _genero;
		
		private string _Cargo;
		
		private decimal _sueldo;
		
		private int _estado;
		
		public sp_perfEmpNombreResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaIngreso", DbType="DateTime NOT NULL")]
		public System.DateTime fechaIngreso
		{
			get
			{
				return this._fechaIngreso;
			}
			set
			{
				if ((this._fechaIngreso != value))
				{
					this._fechaIngreso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre2", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre2
		{
			get
			{
				return this._nombre2;
			}
			set
			{
				if ((this._nombre2 != value))
				{
					this._nombre2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido1", DbType="VarChar(100)")]
		public string apellido1
		{
			get
			{
				return this._apellido1;
			}
			set
			{
				if ((this._apellido1 != value))
				{
					this._apellido1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido2", DbType="VarChar(100)")]
		public string apellido2
		{
			get
			{
				return this._apellido2;
			}
			set
			{
				if ((this._apellido2 != value))
				{
					this._apellido2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaNacimiento", DbType="DateTime")]
		public System.Nullable<System.DateTime> fechaNacimiento
		{
			get
			{
				return this._fechaNacimiento;
			}
			set
			{
				if ((this._fechaNacimiento != value))
				{
					this._fechaNacimiento = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="Int")]
		public System.Nullable<int> telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dui", DbType="VarChar(20)")]
		public string dui
		{
			get
			{
				return this._dui;
			}
			set
			{
				if ((this._dui != value))
				{
					this._dui = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="VarChar(30)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isss", DbType="VarChar(30)")]
		public string isss
		{
			get
			{
				return this._isss;
			}
			set
			{
				if ((this._isss != value))
				{
					this._isss = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_afp", DbType="VarChar(30)")]
		public string afp
		{
			get
			{
				return this._afp;
			}
			set
			{
				if ((this._afp != value))
				{
					this._afp = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_genero", DbType="Int")]
		public System.Nullable<int> genero
		{
			get
			{
				return this._genero;
			}
			set
			{
				if ((this._genero != value))
				{
					this._genero = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cargo", DbType="VarChar(40) NOT NULL", CanBeNull=false)]
		public string Cargo
		{
			get
			{
				return this._Cargo;
			}
			set
			{
				if ((this._Cargo != value))
				{
					this._Cargo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sueldo", DbType="Money NOT NULL")]
		public decimal sueldo
		{
			get
			{
				return this._sueldo;
			}
			set
			{
				if ((this._sueldo != value))
				{
					this._sueldo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_estado", DbType="Int NOT NULL")]
		public int estado
		{
			get
			{
				return this._estado;
			}
			set
			{
				if ((this._estado != value))
				{
					this._estado = value;
				}
			}
		}
	}
	
	public partial class sp_verEmpleadosResult
	{
		
		private string _codigoEmpleado;
		
		private System.DateTime _fechaIngreso;
		
		private string _nombre;
		
		private string _nombre2;
		
		private string _apellido1;
		
		private string _apellido2;
		
		private System.Nullable<System.DateTime> _fechaNacimiento;
		
		private System.Nullable<int> _telefono;
		
		private string _dui;
		
		private string _nit;
		
		private string _isss;
		
		private string _afp;
		
		private System.Nullable<int> _genero;
		
		private string _Cargo;
		
		private decimal _sueldo;
		
		private int _estado;
		
		public sp_verEmpleadosResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codigoEmpleado", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string codigoEmpleado
		{
			get
			{
				return this._codigoEmpleado;
			}
			set
			{
				if ((this._codigoEmpleado != value))
				{
					this._codigoEmpleado = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaIngreso", DbType="Date NOT NULL")]
		public System.DateTime fechaIngreso
		{
			get
			{
				return this._fechaIngreso;
			}
			set
			{
				if ((this._fechaIngreso != value))
				{
					this._fechaIngreso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre2", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string nombre2
		{
			get
			{
				return this._nombre2;
			}
			set
			{
				if ((this._nombre2 != value))
				{
					this._nombre2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido1", DbType="VarChar(100)")]
		public string apellido1
		{
			get
			{
				return this._apellido1;
			}
			set
			{
				if ((this._apellido1 != value))
				{
					this._apellido1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido2", DbType="VarChar(100)")]
		public string apellido2
		{
			get
			{
				return this._apellido2;
			}
			set
			{
				if ((this._apellido2 != value))
				{
					this._apellido2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fechaNacimiento", DbType="Date")]
		public System.Nullable<System.DateTime> fechaNacimiento
		{
			get
			{
				return this._fechaNacimiento;
			}
			set
			{
				if ((this._fechaNacimiento != value))
				{
					this._fechaNacimiento = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="Int")]
		public System.Nullable<int> telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dui", DbType="VarChar(50)")]
		public string dui
		{
			get
			{
				return this._dui;
			}
			set
			{
				if ((this._dui != value))
				{
					this._dui = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="VarChar(50)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isss", DbType="VarChar(50)")]
		public string isss
		{
			get
			{
				return this._isss;
			}
			set
			{
				if ((this._isss != value))
				{
					this._isss = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_afp", DbType="VarChar(50)")]
		public string afp
		{
			get
			{
				return this._afp;
			}
			set
			{
				if ((this._afp != value))
				{
					this._afp = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_genero", DbType="Int")]
		public System.Nullable<int> genero
		{
			get
			{
				return this._genero;
			}
			set
			{
				if ((this._genero != value))
				{
					this._genero = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cargo", DbType="VarChar(40) NOT NULL", CanBeNull=false)]
		public string Cargo
		{
			get
			{
				return this._Cargo;
			}
			set
			{
				if ((this._Cargo != value))
				{
					this._Cargo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sueldo", DbType="Money NOT NULL")]
		public decimal sueldo
		{
			get
			{
				return this._sueldo;
			}
			set
			{
				if ((this._sueldo != value))
				{
					this._sueldo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_estado", DbType="Int NOT NULL")]
		public int estado
		{
			get
			{
				return this._estado;
			}
			set
			{
				if ((this._estado != value))
				{
					this._estado = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
