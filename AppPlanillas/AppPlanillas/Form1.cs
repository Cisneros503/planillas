﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppPlanillas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                //proceso de autenticacion
                UsuariosDataContext db = new UsuariosDataContext();
                var existe = db.sp_login(txtUsr.Text, txtPass.Text);
                //string par3;
                //par3 = " ";
                //var codUsr = db.sp_codUsr(txtUsr.Text, txtPass.Text, ref par3);
                switch (existe.ToString())
                {
                    case "0":
                        MessageBox.Show("No se ha podido autenticar el usuario");
                        break;
                    case "1":
                        MessageBox.Show("Advertencia este perfil tiene altos privilegios sobre el sistema sea cuidadoso con la informacion que va a modificar");
                        MDIadmin adm = new MDIadmin(txtUsr.Text);
                        adm.Show();
                        this.Hide();
                        break;
                    case "2":
                        MDIusuario1 usuario = new MDIusuario1(txtUsr.Text);
                        MessageBox.Show("cod Usr : " + txtUsr.Text);
                        usuario.Show();
                        this.Hide();
                        break;
                    case "3":
                        MDIplanillero planilero = new MDIplanillero(txtUsr.Text);
                        planilero.Show();
                        this.Hide();
                        break;
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
