﻿namespace AppPlanillas
{
    partial class Perfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.planillasNVDataSet = new AppPlanillas.PlanillasNVDataSet();
            this.empleadoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.empleadoTableAdapter = new AppPlanillas.PlanillasNVDataSetTableAdapters.EmpleadoTableAdapter();
            this.txtBuscarNombre = new System.Windows.Forms.TextBox();
            this.btnBuscarNombre = new System.Windows.Forms.Button();
            this.btnBuscarCodEmp = new System.Windows.Forms.Button();
            this.txtBuscarCodEmp = new System.Windows.Forms.TextBox();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEditEstadoCod = new System.Windows.Forms.TextBox();
            this.btnCambiarEst = new System.Windows.Forms.Button();
            this.perfilesDataContextBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.perfilesDataContextBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.perfilesDataContextBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planillasNVDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empleadoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(199, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Perfiles de Empleados:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(18, 140);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(826, 289);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // planillasNVDataSet
            // 
            this.planillasNVDataSet.DataSetName = "PlanillasNVDataSet";
            this.planillasNVDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // empleadoBindingSource
            // 
            this.empleadoBindingSource.DataMember = "Empleado";
            this.empleadoBindingSource.DataSource = this.planillasNVDataSet;
            // 
            // empleadoTableAdapter
            // 
            this.empleadoTableAdapter.ClearBeforeFill = true;
            // 
            // txtBuscarNombre
            // 
            this.txtBuscarNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarNombre.Location = new System.Drawing.Point(18, 96);
            this.txtBuscarNombre.Name = "txtBuscarNombre";
            this.txtBuscarNombre.Size = new System.Drawing.Size(208, 26);
            this.txtBuscarNombre.TabIndex = 2;
            // 
            // btnBuscarNombre
            // 
            this.btnBuscarNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarNombre.Location = new System.Drawing.Point(18, 54);
            this.btnBuscarNombre.Name = "btnBuscarNombre";
            this.btnBuscarNombre.Size = new System.Drawing.Size(208, 35);
            this.btnBuscarNombre.TabIndex = 3;
            this.btnBuscarNombre.Text = "Buscar por nombre";
            this.btnBuscarNombre.UseVisualStyleBackColor = true;
            this.btnBuscarNombre.Click += new System.EventHandler(this.btnBuscarNombre_Click);
            // 
            // btnBuscarCodEmp
            // 
            this.btnBuscarCodEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarCodEmp.Location = new System.Drawing.Point(249, 55);
            this.btnBuscarCodEmp.Name = "btnBuscarCodEmp";
            this.btnBuscarCodEmp.Size = new System.Drawing.Size(288, 35);
            this.btnBuscarCodEmp.TabIndex = 4;
            this.btnBuscarCodEmp.Text = "Buscar po codigo de empleado";
            this.btnBuscarCodEmp.UseVisualStyleBackColor = true;
            this.btnBuscarCodEmp.Click += new System.EventHandler(this.btnBuscarCodEmp_Click);
            // 
            // txtBuscarCodEmp
            // 
            this.txtBuscarCodEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarCodEmp.Location = new System.Drawing.Point(249, 96);
            this.txtBuscarCodEmp.Name = "txtBuscarCodEmp";
            this.txtBuscarCodEmp.Size = new System.Drawing.Size(288, 26);
            this.txtBuscarCodEmp.TabIndex = 5;
            // 
            // btnActualizar
            // 
            this.btnActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.Location = new System.Drawing.Point(564, 87);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(175, 35);
            this.btnActualizar.TabIndex = 6;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 463);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Editar estado del usuario";
            // 
            // txtEditEstadoCod
            // 
            this.txtEditEstadoCod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEditEstadoCod.Location = new System.Drawing.Point(273, 457);
            this.txtEditEstadoCod.Name = "txtEditEstadoCod";
            this.txtEditEstadoCod.Size = new System.Drawing.Size(175, 26);
            this.txtEditEstadoCod.TabIndex = 9;
            // 
            // btnCambiarEst
            // 
            this.btnCambiarEst.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambiarEst.Location = new System.Drawing.Point(624, 450);
            this.btnCambiarEst.Name = "btnCambiarEst";
            this.btnCambiarEst.Size = new System.Drawing.Size(175, 35);
            this.btnCambiarEst.TabIndex = 10;
            this.btnCambiarEst.Text = "Cambiar";
            this.btnCambiarEst.UseVisualStyleBackColor = true;
            this.btnCambiarEst.Click += new System.EventHandler(this.btnCambiarEst_Click);
            // 
            // perfilesDataContextBindingSource
            // 
            this.perfilesDataContextBindingSource.DataSource = typeof(AppPlanillas.PerfilesDataContext);
            // 
            // perfilesDataContextBindingSource1
            // 
            this.perfilesDataContextBindingSource1.DataSource = typeof(AppPlanillas.PerfilesDataContext);
            // 
            // perfilesDataContextBindingSource2
            // 
            this.perfilesDataContextBindingSource2.DataSource = typeof(AppPlanillas.PerfilesDataContext);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.comboBox1.Location = new System.Drawing.Point(469, 457);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 28);
            this.comboBox1.TabIndex = 11;
            // 
            // Perfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Olive;
            this.ClientSize = new System.Drawing.Size(856, 559);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnCambiarEst);
            this.Controls.Add(this.txtEditEstadoCod);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.txtBuscarCodEmp);
            this.Controls.Add(this.btnBuscarCodEmp);
            this.Controls.Add(this.btnBuscarNombre);
            this.Controls.Add(this.txtBuscarNombre);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Name = "Perfil";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Perfil";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Perfil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planillasNVDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empleadoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.perfilesDataContextBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private PlanillasNVDataSet planillasNVDataSet;
        private System.Windows.Forms.BindingSource empleadoBindingSource;
        private PlanillasNVDataSetTableAdapters.EmpleadoTableAdapter empleadoTableAdapter;
        private System.Windows.Forms.TextBox txtBuscarNombre;
        private System.Windows.Forms.BindingSource perfilesDataContextBindingSource;
        private System.Windows.Forms.BindingSource perfilesDataContextBindingSource1;
        private System.Windows.Forms.BindingSource perfilesDataContextBindingSource2;
        private System.Windows.Forms.Button btnBuscarNombre;
        private System.Windows.Forms.Button btnBuscarCodEmp;
        private System.Windows.Forms.TextBox txtBuscarCodEmp;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEditEstadoCod;
        private System.Windows.Forms.Button btnCambiarEst;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}