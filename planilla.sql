USE [master]
GO
/****** Object:  Database [PlanillasNV]    Script Date: 2017-12-09 20:11:19 ******/
CREATE DATABASE [PlanillasNV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PlanillasNV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PlanillasNV.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PlanillasNV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PlanillasNV_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PlanillasNV] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PlanillasNV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PlanillasNV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PlanillasNV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PlanillasNV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PlanillasNV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PlanillasNV] SET ARITHABORT OFF 
GO
ALTER DATABASE [PlanillasNV] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PlanillasNV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PlanillasNV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PlanillasNV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PlanillasNV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PlanillasNV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PlanillasNV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PlanillasNV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PlanillasNV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PlanillasNV] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PlanillasNV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PlanillasNV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PlanillasNV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PlanillasNV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PlanillasNV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PlanillasNV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PlanillasNV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PlanillasNV] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PlanillasNV] SET  MULTI_USER 
GO
ALTER DATABASE [PlanillasNV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PlanillasNV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PlanillasNV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PlanillasNV] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PlanillasNV] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PlanillasNV]
GO
/****** Object:  Table [dbo].[Cargo]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cargo](
	[idCargo] [int] NOT NULL,
	[nombreCargo] [varchar](100) NOT NULL,
	[sueldoBase] [money] NOT NULL,
	[Descripcion] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DescuentosLey]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DescuentosLey](
	[idDesc] [int] NOT NULL,
	[nombreDesc] [varchar](100) NULL,
	[montoDesc] [money] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[codigoEmpleado] [varchar](50) NOT NULL,
	[fechaIngreso] [date] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[nombre2] [varchar](100) NOT NULL,
	[apellido1] [varchar](100) NULL,
	[apellido2] [varchar](100) NULL,
	[fechaNacimiento] [date] NULL,
	[telefono] [int] NULL,
	[dui] [varchar](50) NULL,
	[nit] [varchar](50) NULL,
	[isss] [varchar](50) NULL,
	[afp] [varchar](50) NULL,
	[genero] [int] NULL,
	[Cargo] [varchar](40) NOT NULL,
	[sueldo] [money] NOT NULL,
	[estado] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OtrosDescuentos]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OtrosDescuentos](
	[id] [int] NOT NULL,
	[codigoEmpleado] [varchar](50) NOT NULL,
	[nombre] [varchar](100) NULL,
	[montoDesc] [money] NOT NULL,
	[plazo] [int] NOT NULL,
	[estado] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OtrosIngresos]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OtrosIngresos](
	[id] [int] NOT NULL,
	[codigoEmpleado] [varchar](50) NOT NULL,
	[nombre] [varchar](200) NOT NULL,
	[montoIngreso] [money] NOT NULL,
	[plazo] [int] NOT NULL,
	[estado] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Planilla]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planilla](
	[id] [int] NOT NULL,
	[codigoEmpleado] [varchar](50) NOT NULL,
	[fechaPlanillaInicio] [datetime] NOT NULL,
	[fechaPlanillaFin] [datetime] NOT NULL,
	[tipoPlanilla] [varchar](25) NOT NULL,
	[Salario] [money] NULL,
	[DescuentosLey] [money] NOT NULL,
	[OtrosDescuentos] [money] NULL,
	[OtrosIngresos] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[codigoEmpleado] [varchar](50) NOT NULL,
	[usuario] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[nombre] [varchar](200) NOT NULL,
	[rol] [varchar](25) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[editarEstado]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[editarEstado]
(@est int, @cod varchar (25))
as 
begin 
update Empleado
set
	estado = @est 
where codigoEmpleado = @Cod
end
GO
/****** Object:  StoredProcedure [dbo].[sp_codUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_codUsr] (@usr varchar(100), @pwd varchar(100),  @codi varchar(25) output)
as
begin
	--set @resultado = (select permisos from usuarios where usuario=@usr and password=@pwd);
	set @codi = (select codigoEmpleado from usuario where usuario=@usr and password=@pwd);
	return 1	
end

GO
/****** Object:  StoredProcedure [dbo].[sp_codUsrLog]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_codUsrLog] @usr varchar(100), @pwd varchar(100)
as
begin
declare
	@cod varchar(25),
	@resultado varchar(8),
	@devolucion int
	--set @resultado = (select permisos from usuarios where usuario=@usr and password=@pwd);
	set @resultado = (select rol from usuario where usuario=@usr and password=@pwd);
	set @cod = (select codigoEmpleado from usuario where usuario=@usr and password=@pwd);
	if @resultado like 'admin'
	begin
		set @devolucion = 1
	end	
	else 
		if @resultado like 'usuario'
		begin
			set @devolucion = 2
		end
		else 
			if @resultado like 'conta'
			begin
				set @devolucion = 3
			end
			else
				set @devolucion = 0

	return @devolucion
	return @cod
end
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteCargo]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_deleteCargo]  
@nombreCargo varchar(50)
as
begin 
delete from Cargo where nombreCargo = @nombreCargo
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDescLey]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_deleteDescLey] 
@idDesc int
as
begin 
if exists (select * from dbo.DescuentosLey where idDesc = @idDesc)
 begin 
	 delete from DescuentosLey where idDesc = @idDesc
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteEmp]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_deleteEmp] (@codEmp varchar (25))
as
begin
	DELETE FROM Empleado where codigoEmpleado = @codEmp
end

GO
/****** Object:  StoredProcedure [dbo].[sp_deleteOtrosDesc]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_deleteOtrosDesc] 
@idDesc int
as
begin 
if exists (select * from dbo.OtrosDescuentos where id = @idDesc )
 begin 
 delete from dbo.OtrosDescuentos where id = @idDesc
 end
else
 begin
 raiserror('Error -998: El registro no existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteOtrosIngre]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_deleteOtrosIngre] 
@idIngreso int
as
begin 
if exists (select * from dbo.OtrosIngresos where id = @idIngreso )
 begin
 delete from dbo.OtrosIngresos where id = @idIngreso
 end
else
begin 
  raiserror('Error -998: El registro NO existe.', 1, 10)
 return -998
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_deletePlanillaXID]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_deletePlanillaXID] 
@id int
as
begin 
if exists (select * from dbo.Planilla where id = @id)
 begin 
 delete from dbo.Planilla where id = @id
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_deleteUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[sp_deleteUsr] 
@codEmp varchar(25)
as
begin 
if exists (select * from dbo.Usuario where codigoEmpleado = @codEmp)
 begin 
	delete from Usuario where codigoEmpleado = @codEmp
 end
else
 begin
	raiserror('Error : El registro no existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_editOtrosDesc]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_editOtrosDesc] 
@codEmp varchar(50), @idDesc int, @nomDesc varchar(100), @montoDesc money , @plazo int, @estado int
as
begin 
if exists (select * from dbo.OtrosDescuentos where id = @idDesc )
 begin 
 update dbo.OtrosDescuentos
 set
 id = @idDesc,
 codigoEmpleado = @codEmp, 
 nombre = @nomDesc, 
 montoDesc = @montoDesc, 
 plazo = @plazo, 
 estado = @estado
 where id = @idDesc
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_EditUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_EditUsr] 
@codEmp varchar(50),
@usr varchar(50),
@pass varchar(50),
@nom varchar(50),
@rol varchar(25)
as
begin 
if exists (select * from dbo.Usuario where codigoEmpleado = @codEmp)
 begin 
 update dbo.Usuario
 set 
 codigoEmpleado = @codEmp,
 usuario = @usr,
 password = @pass,
 nombre =  @nom ,
 rol = @rol
 where codigoEmpleado = @codEmp
 
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_empleados]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_empleados] 
as
begin
	select * from Empleado	
end
GO
/****** Object:  StoredProcedure [dbo].[sp_insertCargo]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_insertCargo] 
@idCargo int, @nombreCargo varchar(50), @sueldoBase money, @descripcion varchar (max)
as
begin 
if exists (select * from dbo.Cargo where nombreCargo = @nombreCargo)
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 
 Insert into dbo.Cargo ( idCargo, nombreCargo, sueldoBase, Descripcion )
 Values (@idCargo , @nombreCargo , @sueldoBase , @descripcion )

 end

return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insertDescLey]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_insertDescLey] 
@idDesc int, @nomDesc varchar(100), @montoDesc money
as
begin 
if exists (select * from dbo.DescuentosLey where nombreDesc = @nomDesc)
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 
 Insert into dbo.DescuentosLey ( idDesc, nombreDesc, montoDesc )
 Values (@idDesc, @nomDesc, @montoDesc )

 end

return 0
end


GO
/****** Object:  StoredProcedure [dbo].[sp_insertEmpleado]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_insertEmpleado] 
@codEmp varchar(20), @fechaIngreso datetime, 
@nombreEmp varchar(50), @nombreEmp2 varchar(50), @apellido1 varchar (50), @apel2 varchar(50),@fNac datetime ,@telefono int, 
@dui varchar(50), @nit varchar(50), @isss varchar(50), @afp varchar(50), @genero int, @cargo varchar (25),  @sueldo money, @estado int  
as
begin 
if exists (select * from dbo.Empleado where dbo.Empleado.codigoEmpleado = @codEmp)
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 Insert into dbo.Empleado
 (codigoEmpleado, fechaIngreso, nombre, nombre2,apellido1,apellido2, fechaNacimiento, telefono,
 dui, nit, isss, afp, genero, Cargo, sueldo, estado )
 Values 
 ( @codEmp, @fechaIngreso, @nombreEmp,@nombreEmp2, @apellido1, @apel2, @fNac, @telefono, 
	@dui, @nit, @isss, @afp, @genero, @cargo, @sueldo, @estado)
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insertOtrosDesc]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_insertOtrosDesc] 
@codEmp varchar(50), @idDesc int, @nomDesc varchar(100), @montoDesc money , @plazo int, @estado int
as
begin 
if exists (select * from dbo.OtrosDescuentos where id = @idDesc )
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 
 Insert into dbo.OtrosDescuentos ( id, codigoEmpleado, nombre, montoDesc, plazo, estado )
 Values (@idDesc,@codEmp, @nomDesc, @montoDesc, @plazo, @estado )

 end

return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insertOtrosIngre]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_insertOtrosIngre] 
@codEmp varchar(50), @idIngreso int, @nomIngr varchar(100), @montoIngr money , @plazo int, @estado int
as
begin 
if exists (select * from dbo.OtrosIngresos where id = @idIngreso )
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 Insert into dbo.OtrosIngresos( id, codigoEmpleado, nombre, montoIngreso, plazo, estado )
 Values (@idIngreso,@codEmp, @nomIngr, @montoIngr, @plazo, @estado )
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insertPlanilla]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_insertPlanilla] 
@id int, @codEmp varchar(50), @fechaIni datetime, @fechafin datetime, 
@tipoPlanilla varchar(75), @salario money, @descLey money, @otrosDesc money, @otrosIng money
as
begin 
if exists (select * from dbo.Planilla where id = @id)
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 
 Insert into dbo.Planilla( id, codigoEmpleado, fechaPlanillaInicio, fechaPlanillaFin, tipoPlanilla, Salario, DescuentosLey, 
 OtrosDescuentos, OtrosIngresos)
 Values (@id, @codEmp, @fechaIni, @fechafin, @tipoPlanilla, @salario, @descLey, @otrosDesc, @otrosIng )

 end

return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insertUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_insertUsr] 
@codEmp varchar(50),
@usr varchar(50),
@pass varchar(50),
@nom varchar(50),
@rol varchar(25)
as
begin 
if exists (select * from dbo.Usuario where codigoEmpleado = @codEmp)
 begin 
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
else
 begin
 Insert into dbo.Usuario( codigoEmpleado, usuario, password, nombre, rol )
 Values (@codEmp , @usr, @pass , @nom , @rol )
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listarDescLey]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_listarDescLey] 
as
begin 
select * from DescuentosLey
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_listarOtrosDesc]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listarOtrosDesc] 
as
begin 
select * from OtrosDescuentos
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listCargos]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listCargos]
as
begin
	select * from Cargo
end


GO
/****** Object:  StoredProcedure [dbo].[sp_listOtrosDescXId]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listOtrosDescXId] @id int
as
begin 
select * from OtrosDescuentos where id = @id
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listOtrosDescXU]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listOtrosDescXU] 
@codEmp varchar (50)
as
begin 
select * from OtrosDescuentos where codigoEmpleado = @codEmp
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listOtrosIngre]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listOtrosIngre] 
as
begin 
select * from dbo.OtrosIngresos
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_listOtrosIngreXID]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listOtrosIngreXID] 
@id int
as
begin 
if exists (select * from dbo.OtrosIngresos where id = @id )
 begin
select * from dbo.OtrosIngresos where id = @id
 end
else
begin 
  raiserror('Error -998: El registro NO existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listOtrosIngreXU]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_listOtrosIngreXU] 
@codEmp varchar(50)
as
begin 
if exists (select * from dbo.OtrosIngresos where codigoEmpleado = @codEmp )
 begin
select * from dbo.OtrosIngresos where codigoEmpleado = @codEmp
 end
else
begin 
  raiserror('Error -998: El registro NO existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listPlanillaXFECH]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listPlanillaXFECH] 
 @fechaIni datetime
as
begin 
if exists (select * from dbo.Planilla where fechaPlanillaInicio = @fechaIni)
 begin 
 select * from dbo.Planilla where fechaPlanillaInicio = @fechaIni
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end

return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_listPlanillaXID]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listPlanillaXID] 
@id int
as
begin 
if exists (select * from dbo.Planilla where id = @id)
 begin 
 select * from dbo.Planilla where id = @id
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listPlanillaXU]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listPlanillaXU] 
@codEmp varchar(50)
as
begin 
if exists (select * from dbo.Planilla where codigoEmpleado = @codEmp)
 begin 
 select * from dbo.Planilla where codigoEmpleado = @codEmp
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_listUsr]
as
begin
	select * from Usuario
end
GO
/****** Object:  StoredProcedure [dbo].[sp_login]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_login] @usr varchar(100), @pwd varchar(100)
as
begin
declare
	@resultado varchar(8),
	@devolucion int
	--set @resultado = (select permisos from usuarios where usuario=@usr and password=@pwd);
	set @resultado = (select rol from usuario where codigoEmpleado=@usr and password=@pwd);
	if @resultado like 'admin'
	begin
		set @devolucion = 1
	end	
	else 
		if @resultado like 'usuario'
		begin
			set @devolucion = 2
		end
		else 
			if @resultado like 'conta'
			begin
				set @devolucion = 3
			end
			else
				set @devolucion = 0

	return @devolucion
end
GO
/****** Object:  StoredProcedure [dbo].[sp_perfEmpleado]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_perfEmpleado] (@codEmp varchar (25))
as
begin
	select * from Empleado where codigoEmpleado = @codEmp
end
GO
/****** Object:  StoredProcedure [dbo].[sp_perfEmpNombre]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_perfEmpNombre] (@nomEmp varchar (100))
as
begin
	select * from Empleado where nombre like @nomEmp + '%'
end
GO
/****** Object:  StoredProcedure [dbo].[sp_selectUsr]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[sp_selectUsr] (@codEmp varchar (25))
as
begin
	select * from Usuario where codigoEmpleado = @codEmp
end
GO
/****** Object:  StoredProcedure [dbo].[sp_updateCargo]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_updateCargo] 
@idCargo int , 
@nombreCargo varchar(50), 
@sueldoBase money, 
@descripcion varchar (max)
as
begin 
update Cargo
	set
	idCargo = @idCargo,
	nombreCargo = @nombreCargo,
	sueldoBase = @sueldoBase,
	Descripcion = @descripcion
	where nombreCargo = @nombreCargo
return 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDescLey]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_updateDescLey] 
@idDesc int, @nomDesc varchar (100), @montoDesc money
as
begin 
if exists (select * from dbo.DescuentosLey where idDesc = @idDesc)
 begin 
 update dbo.DescuentosLey
 set
 idDesc = @idDesc,
 nombreDesc = @nomDesc,
 montoDesc = @montoDesc 
where idDesc = @idDesc
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_updateEmp]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_updateEmp] 
@codEmp varchar(20), @fechaIngreso datetime, 
@nombreEmp varchar(50), @nombreEmp2 varchar(50), 
@apellido1 varchar (50), @apel2 varchar(50),
@fNac datetime ,@telefono int, @dui varchar(50), 
@nit varchar(50), @isss varchar(50), @afp varchar(50), 
@genero int, @cargo varchar (25),  @sueldo money, @estado int  
as 
begin
update dbo.Empleado 
set  
 codigoEmpleado = @codEmp,
 fechaIngreso = @fechaIngreso,
 nombre = @nombreEmp,
 nombre2 = @nombreEmp2,
 apellido1 = @apellido1,
 apellido2 = @apel2,
 fechaNacimiento = @fNac,
 telefono = @telefono,
 dui = @dui,
 nit = @nit,
 isss = @isss,
 afp = @afp,
 genero = @genero,
 Cargo = @cargo,
 sueldo = @sueldo,
 estado = @estado
where codigoEmpleado = @codEmp 
end
GO
/****** Object:  StoredProcedure [dbo].[sp_updateOtrosIngre]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_updateOtrosIngre] 
@codEmp varchar(50), @idIngreso int, @nomIngr varchar(100), @montoIngr money , @plazo int, @estado int
as
begin 
if exists (select * from dbo.OtrosIngresos where id = @idIngreso )
 begin
 update dbo.OtrosIngresos
 set
 id = @idIngreso, 
 codigoEmpleado = @codEmp, 
 nombre = @nomIngr, 
 montoIngreso = @montoIngr, 
 plazo = @plazo, 
 estado = @estado
 where id = @idIngreso
 end
else
begin 
  raiserror('Error -998: El registro NO existe.', 1, 10)
 return -998
 end
return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_updatePlanilla]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_updatePlanilla] 
@id int, @codEmp varchar(50), @fechaIni datetime, @fechafin datetime, 
@tipoPlanilla varchar(75), @salario money, @descLey money, @otrosDesc money, @otrosIng money
as
begin 
if exists (select * from dbo.Planilla where id = @id)
 begin 
 update dbo.Planilla
 set 
 codigoEmpleado = @codEmp, 
 fechaPlanillaInicio = @fechaIni, 
 fechaPlanillaFin = @fechafin, 
 tipoPlanilla = @tipoPlanilla, 
 Salario = @salario, 
 DescuentosLey = @descLey, 
 OtrosDescuentos = @otrosDesc, 
 OtrosIngresos = @otrosIng
 where id = @id
 end
else
 begin
 raiserror('Error -998: El registro ya existe.', 1, 10)
 return -998
 end

return 0
end

GO
/****** Object:  StoredProcedure [dbo].[sp_verCargo]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_verCargo] (@carg varchar (50))
as
begin
	select * from Cargo where nombreCargo like @carg + '%'
end

GO
/****** Object:  StoredProcedure [dbo].[sp_verEmpleados]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_verEmpleados]
as
begin
	select * from Empleado
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verPlanilla]    Script Date: 2017-12-09 20:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_verPlanilla] 
as
begin 
 select * from dbo.Planilla
return 0
end
GO
USE [master]
GO
ALTER DATABASE [PlanillasNV] SET  READ_WRITE 
GO
